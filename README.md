# Car Info - .NET Agent
Program wykonuje symulacje pracy silnika samochodu podczas przejazdów testowych na torze wyścigowym. 
Wszystkie dane zaczerpnięte z [http://racerender.com](http://racerender.com/Developer/DataFormat.html/)

### Framework & narzędzia
```
* .NET Core 2.0
* Visual Studio 2017
```

### Konfiguracja
Konfiguracja połączenia np zmiana portu możliwa w pliku appsettings.json

[Zdjecie pogladowe](http://prntscr.com/huxc09)


