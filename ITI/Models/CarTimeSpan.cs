﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ITI.Models
{
    public class CarTimeSpan
    {
        public string Gps_Latitude { get; set; }
        public string Gps_Longitude { get; set; }
        public string Speed_MPH { get; set; }
        public string AcceleratorPedalPosition { get; set; }
        public string Time { get; set; }

        public Engine CarEngine { get; set; }

        public CarTimeSpan(string gps_Latitude, string gps_Longitude, string speed_MPH, string acceleratorPedalPosition, string time, string revolutions)
        {
            this.CarEngine = new Engine();
            this.Gps_Latitude = gps_Latitude;
            this.Gps_Longitude = gps_Longitude;
            this.Speed_MPH = speed_MPH;
            this.AcceleratorPedalPosition = acceleratorPedalPosition;
            this.Time = time;
            this.CarEngine.Revolutions = revolutions;
        }

        public void UpdareValues(string gps_Latitude, string gps_Longitude, string speed_MPH, string acceleratorPedalPosition, string time, string revolutions)
        {
            this.Gps_Latitude = gps_Latitude;
            this.Gps_Longitude = gps_Longitude;
            this.Speed_MPH = speed_MPH;
            this.AcceleratorPedalPosition = acceleratorPedalPosition;
            this.Time = time;
            this.CarEngine.Revolutions = revolutions;
        }

        public string Speed_KMH
        {
            get
            {
                return Math.Round((double.Parse(this.Speed_MPH) * 1.61), 0).ToString();
            }
        }

    }
}
