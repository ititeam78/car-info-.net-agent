﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ITI.Models
{
    public class Engine
    {
        public string Temperature { get; set; }
        public string Revolutions { get; set; }

        private Random Random;

        public Engine()
        {
            this.Random = new Random();
            this.GenerateRandTemp(70, 100);
        }

        public void UpdateParameters(string revolutions)
        {
            this.GenerateRandTemp(70, 100);
            this.Revolutions = revolutions;

        }

        public void GenerateRandTemp(double minValue, double maxValue)
        {
           this.Temperature = (this.Random.NextDouble() * (maxValue - minValue) + minValue).ToString();
        }
    }
}
