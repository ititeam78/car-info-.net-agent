﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ITI.Models
{
    public interface ICarTimeSpanRepository
    {
        IQueryable<CarTimeSpan> CarTimeSpans { get; }
    }
}
