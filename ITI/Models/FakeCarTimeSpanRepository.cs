﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ITI.Models
{
    public class FakeCarTimeSpanRepository : ICarTimeSpanRepository
    {
        public IQueryable<CarTimeSpan> CarTimeSpans =>
            this.FakeValues().AsQueryable();


        public List<CarTimeSpan> FakeValues()
        {
            List<CarTimeSpan> list = new List<CarTimeSpan>();
            var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "RRData.csv");

            using (var reader = new StreamReader(path))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(',');

                    list.Add(new CarTimeSpan(values[5], values[6], (Math.Round(double.Parse(values[8]), 0)).ToString(), values[15], values[0], (Math.Round(double.Parse(values[14]), 0)).ToString()));
                }
            }

            return list;
        }
    }
}
