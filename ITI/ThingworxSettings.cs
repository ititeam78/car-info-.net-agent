﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ITI
{
    public class ThingworxSettings
    {
        public string AppKey { get; set; }
        public string BaseAddressURI { get; set; }
        public int RequestSleep { get; set; }
    }
}
