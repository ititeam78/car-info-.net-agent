﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ITI.Models;
using System.IO;
using System.Threading;
using System.Net.Http;
using System.Text;
using System.Net.Http.Headers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace ITI.Controllers
{
    public class HomeController : Controller
    {
        private readonly IOptions<ThingworxSettings> thingworxSettings;
        private ICarTimeSpanRepository repository;

        public HomeController(ICarTimeSpanRepository repo, IOptions<ThingworxSettings> _thingworxSettings)
        {
            this.repository = repo;
            this.thingworxSettings = _thingworxSettings;
        }


        public IActionResult Index()
        {
            return RedirectToAction("Data");
        }

        public IActionResult Data() => View(repository.CarTimeSpans);

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public async Task StartEngine()
        {
            List<CarTimeSpan> cartimespans = repository.CarTimeSpans.ToList();

            string param, uriPath;

            for (int i = 0; i < repository.CarTimeSpans.Count(); i++)
            {
                // Car Speed
                param = Newtonsoft.Json.JsonConvert.SerializeObject(new { Value_Property = cartimespans[i].Speed_MPH, });
                uriPath = "Things/Value_Stream_Test_Thing/Properties/Value_Property";
                RequestToThingworx(param, uriPath);

                // Revolutions
                param = Newtonsoft.Json.JsonConvert.SerializeObject(new { Revolutions_Property = cartimespans[i].CarEngine.Revolutions, });
                uriPath = "Things/Value_Stream_Test_Thing/Properties/Revolutions_Property";
                RequestToThingworx(param, uriPath);

                // Car gps position
                param = Newtonsoft.Json.JsonConvert.SerializeObject(new { Location_Property = string.Format("{0},{1},18", cartimespans[i].Gps_Latitude, cartimespans[i].Gps_Longitude) });
                uriPath = "Things/Value_Stream_Test_Thing/Properties/Location_Property";
                RequestToThingworx(param, uriPath);

                i += 25;
            }
        }

        public async Task RequestToThingworx(string paramJson, string uriPath)
        {
            using (var client = new HttpClient())
            {
                var param = paramJson;
                HttpContent contentPost = new StringContent(param, Encoding.UTF8, "application/json");
                client.DefaultRequestHeaders.TryAddWithoutValidation("AppKey", thingworxSettings.Value.AppKey);
                client.DefaultRequestHeaders.TryAddWithoutValidation("Accept", "application/json");
                //client.DefaultRequestHeaders.Add("Content-Type", "application/json");
                
                client.BaseAddress = new Uri(thingworxSettings.Value.BaseAddressURI);
                var response = client.PutAsync(uriPath, contentPost).Result;

                /// TODO: Exceptions
                if (response.IsSuccessStatusCode)
                {
                    Console.Write("Success");
                }
                else
                    Console.Write("Error");
            }

            Thread.Sleep(thingworxSettings.Value.RequestSleep);
        }
    }
}
